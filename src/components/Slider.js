import React from 'react'

export default function Slider({ min, max, value, title, onChange }) {
  return <div>
    <label>
      <strong>{title}</strong><br />
      <input type="range" min={min} max={max} value={value}
        onChange={e => onChange(parseInt(e.target.value))} style={{ width: "25rem" }} />
      {" "}
      {value}
    </label>
  </div>
}